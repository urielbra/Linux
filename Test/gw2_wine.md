====== Information ======

  * Wine Staging ((https://github.com/wine-compholio/wine-patched))
  * Wine Patches ((https://github.com/JKtheSlacker/wine-patches))
  * These notes are slightly-messy and only build 64-bit Wine ((good for Guild Wars 2 and 64-bit only things))

===== Prerequisites =====

  * [[distros:ubuntu_mate | Ubuntu MATE]]

====== Dependencies ======

****

  sudo apt-get install build-essential g++ g++-5 libstdc++-5-dev flex libfl-dev m4 bison libbison-dev libpthread-stubs0-dev libx11-dev libx11-doc libxau-dev libxcb1-dev libxdmcp-dev x11proto-core-dev x11proto-input-dev x11proto-kb-dev xorg-sgml-doctools xtrans-dev libfreetype6-dev libpng12-dev zlib1g-dev libxcursor-dev libxfixes-dev libxrender-dev x11proto-fixes-dev x11proto-render-dev x11proto-xext-dev libxext-dev libxi-dev libxxf86vm-dev x11proto-xf86vidmode-dev libxrandr-dev x11proto-randr-dev libxinerama-dev x11proto-xinerama-dev libxcomposite-dev x11proto-composite-dev libx11-xcb-dev libdrm-dev libgl1-mesa-dev libglu1-mesa-dev libxcb-dri2-0-dev libxcb-dri3-dev libxcb-glx0-dev libxcb-present-dev libxcb-randr0-dev libxcb-render0-dev libxcb-shape0-dev libxcb-sync-dev libxcb-xfixes0-dev libxdamage-dev libxshmfence-dev mesa-common-dev x11proto-damage-dev x11proto-dri2-dev x11proto-gl-dev libosmesa6-dev libset-scalar-perl libva-dev libva-egl1 libva-glx1 libva-tpi1 libwayland-dev nvidia-opencl-dev opencl-headers libpcap-dev libpcap0.8-dev libdbus-1-dev libncurses5-dev libtinfo-dev libsane-dev libv4l-dev libv4l2rds0 javascript-common libexif-dev libgphoto2-dev libjs-jquery liblcms2-dev autotools-dev debhelper dh-strip-nondeterminism gettext intltool-debian libasprintf-dev libatk-bridge2.0-dev libatk1.0-dev libatspi2.0-dev libcairo-script-interpreter2 libcairo2-dev libegl1-mesa-dev libepoxy-dev libexpat1-dev libfile-stripnondeterminism-perl libfontconfig1-dev libgdk-pixbuf2.0-dev libgettextpo-dev libgettextpo0 libglib2.0-dev libgmp-dev libgmpxx4ldbl libgtk-3-dev libharfbuzz-dev libharfbuzz-gobject0 libice-dev libmail-sendmail-perl libmirclient-dev libmircommon-dev libmircookie-dev libmircookie2 libpango1.0-dev libpcre3-dev libpcre32-3 libpcrecpp0v5 libpixman-1-dev libprotobuf-dev libsm-dev libsys-hostname-long-perl libunistring0 libxcb-shm0-dev libxft-dev libxkbcommon-dev libxtst-dev nettle-dev po-debconf x11proto-record-dev libpulse-dev icu-devtools libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev libicu-dev libxml2-dev libudev-dev libcapi20-dev libcups2-dev libgsm1-dev libjbig-dev libjpeg-dev libjpeg-turbo8-dev libjpeg8-dev liblzma-dev libtiff5-dev libtiffxx5 libmpg123-dev libopenal-dev libldap2-dev libxslt1-dev libgnutls-dev libgnutlsxx28 libidn11-dev libp11-kit-dev libtasn1-6-dev libtasn1-doc

====== Apply Patches ======

  cd ~/'Downloads/wine-patched-staging-1.9.19'

  patch -p1 < ~/'Downloads/wine-patches-master/0000-undo-ntdll-Heap-FreeLists.patch'

  patch -p1 < ~/'Downloads/wine-patches-master/0001-ntdll-improve-heap-allocation-performance.patch'

  patch -p1 < ~/'Downloads/wine-patches-master/0002-wined3d-use-SwitchToThread-and-calls-to-select-in-bu.patch'

  patch -p1 < ~/'Downloads/wine-patches-master/0003-ntdll-heap.c-align-everything-to-64-byte-to-reduce-f.patch'

  patch -p1 < ~/'Downloads/wine-patches-master/0004-wine-list.h-linked-list-cache-line-prefetching.patch'

  patch -p1 < ~/'Downloads/wine-patches-master/0005-ntdll-heap.c-freelist_balance-prefetch-next-entry-ca.patch'

  patch -p1 < ~/'Downloads/wine-patches-master/0006-oleaut32-typelib.c-fix-cursor2-having-the-wrong-type.patch' 

====== Compile ======

  export CFLAGS='-O3 -pipe -march=native'

  make clean

  ./configure --enable-win64

  make -j8

====== Fedora Copr ======

  * http://bradthemad.org/tech/notes/patching_rpms.php

  * https://copr.fedorainfracloud.org/coprs/espionage724/wine-patches

===== Dependencies =====

****

  sudo dnf install 'rpm-build'

===== Prepare SRPM =====

  * Acquire SRPM from https://dl.fedoraproject.org/pub/fedora/linux/updates/25/SRPMS/w/

  rm -Rf ~/'rpmbuild' && rpm -ivh ~/'Downloads/wine'*'fc25.src.rpm'

===== Download Patches =====

==== laino/wine-patches ====

****

  wget 'https://github.com/laino/wine-patches/archive/master.zip' -O ~/'rpmbuild/SOURCES/laino-wine-patches.zip' && unzip ~/'rpmbuild/SOURCES/laino-wine-patches.zip' -d ~/'rpmbuild/SOURCES' && mv ~/'rpmbuild/SOURCES/wine-patches-master/'*.'patch' ~/'rpmbuild/SOURCES' && rm -Rf ~/'rpmbuild/SOURCES/wine-patches-master' && sync

===== Spec Config =====

  nano ~/'rpmbuild/SPECS/wine.spec'

  * Add ''.1'' value to ''Release'' ((current release is .1))
  * If new ''Version'' is released, start back at ''.1'' ((current version is 2.0))

  1%{?dist}.1

  * Add under ''Patch511''

  # laino wine-patches
  Patch600:       0001-ntdll-improve-heap-allocation-performance.patch
  Patch601:       0002-ntdll-heap.c-align-everything-to-64-byte-to-reduce-f.patch
  Patch602:       0003-wine-list.h-linked-list-cache-line-prefetching.patch
  Patch603:       0004-ntdll-heap.c-freelist_balance-prefetch-next-entry-ca.patch
  Patch604:       0005-oleaut32-typelib.c-fix-cursor2-having-the-wrong-type.patch
  Patch605:       0006-Ensure-16-byte-alignment-of-data.patch

  * Add under ''%prep'' after the ''make -C patches'' line

  # undo ntdll-Heap_FreeLists
  /usr/bin/patch -p1 -R < patches/ntdll-Heap_FreeLists/0001-ntdll-Improve-heap-allocation-performance-by-using-m.patch
  
  # apply laino wine-patches
  %patch600 -p1
  %patch601 -p1
  %patch602 -p1
  %patch603 -p1
  %patch604 -p1
  %patch605 -p1

  * Add under ''%build''

  # performance
  export CFLAGS="-Ofast -g0 -flto=1 -flto-partition=none -fuse-linker-plugin -fipa-pta"

===== Build SRPM =====

****

  rpmbuild -bs ~/'rpmbuild/SPECS/wine.spec'

===== Compile Locally =====

  sudo dnf builddep ~/'Downloads/wine'*'fc25.src.rpm'

  rpmbuild -ba ~/'rpmbuild/SPECS/wine.spec'